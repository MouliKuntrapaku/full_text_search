# **Full Text Search :** 
* Full text search is the most common technique used in Web search engines and Web pages.
* Each page is searched and indexed, and if any matches are found, they are displayed via the indexes.
* Parts of original text are displayed against the user’s query and then the full text. 
* Full-text search helps in searching for a word in huge amounts of metadata, such as the World Wide Web and commercial-scale databases. 
* Full-text search became popular in late 1990s, when the Internet began to became a part of everyday life.

## **Why Full-Text-Search is more efficient :**
 * A common question from non-Full-Text users is, “If Full-Text search is about looking for words inside text, then XQuery already does that with the contains function. 
 * The contains function does not do a Full-Text search – it does a substring search. 
 * The main difference is that a Full-Text search will generally match only a complete word, and not just part of a string. 

**For example** :

 * A Full-Text search for “dent” will not match a piece of text that contains the word “students”, but a substring search will.
 * Also, when running a Full-Text search, there is generally an assumption that the match will be case-insensitive, so that “dent” will match “DENT” as well as “dent”. 
 * With substring queries, matching is usually case-sensitive, so that the text being searched has to match the case of the search term.

## **Full-Text Search types :**

### **1. A Natural language search type :** 
* This type of search mode interprets the search string as a literal phrase. 
* It is enabled by default if no modifier is specified.
  
### **2. A Query expansion search type :** 
* This type of search mode performs the search twice. 
* When searching the second time, the result set includes a few most relevant documents from the first search.
* It can be enabled using with "QUERY EXPANSION" modifier.

### **3. A Boolean search type :**
* This type of search mode enables searching for complex queries.
* It can include boolean operators such as less than (“<”) and more than (“>”).
* It also includes subexpressions (“(” and “)”), the plus sign (+), the minus sign (-), double quotes (“”).
* The wildcard operator (*) allows searching with fuzzy matching. For example, “demo*” would also match “demonstration”.
* It can be enabled using the "BOOLEAN MODE" modifier.

## **How does Full-Text Search work?**

* The full-text search first starts with a database containing a bunch of data. 
* These data are indexed based on the words in the documents and stored. 
* When a user enters a search query, the query is split into tokens and mapped across the built index to find the relevant documents.

## **Challenges In Full text Search :**

* Scaling    
* Proximity 
* Handling Synonyms, Abbreviations, homonyms, Phonetics, Misspellings
* Searching through images
* Re-indexing
* Full-Text-Search in distributed systems

## **Disadvantages:**

* A fulltext index can potentially be huge, many times larger than a standard B-TREE index. For this reason, many hosted providers who offer database instances disable this feature, or at least charge extra for it. For example, Windows Azure did not support fulltext queries.
  
* Fulltext indexes can also be slower to update. If the data changes a lot, there might be some lag updating indexes compared to standard indexes.

## **Conclusion :**
* Users searching full text are more likely to find relevant articles than searching only abstracts. 
  
* Full text collections for text retrieval and provides a starting point for future work in exploring algorithms that take advantage of rapidly-growing digital archives.
   
* Full-text articles are significantly longer than abstracts and may require the computational resources of multiple machines in a cluster. 


## **Resources :**
* https://en.wikipedia.org/wiki/Full-text_search#:~:text=In%20text%20retrieval%2C%20full%2Dtext,in%20a%20full%2Dtext%20database.&text=In%20a%20full%2Dtext%20search,text%20specified%20by%20a%20user.
* https://www.couchbase.com/products/full-text-search
* https://www.techopedia.com/definition/17113/full-text-search
* https://youtu.be/2OY4tE2TrcI
